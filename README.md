# Cygnus Response Helper

Simple PHP library for formatting responses

#### Requirements:
+ PHP 7.0 or higher
+ Composer (for installation)

#### Features:
+ Converts your data to JSON/XML
+ Standardized responses, such as Success/Fail
+ Functional tests

#### Installation:
```shell script
composer require ruslanty/cygnus-response-helper
```
