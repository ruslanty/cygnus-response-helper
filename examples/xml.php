<?php

declare(strict_types=1);

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
date_default_timezone_set('UTC');

require __DIR__ . '/../vendor/autoload.php';

$response = [
    555,
    'aaa',
    'string' => 'String',
    'int' => 123,
    'float' => 13.13,
    'bool' => false,
    'array' => [
        666,
        'bbb',
        'sub_array' => [
            777,
            'ccc',
        ],
    ],
    'object' => new stdClass(),
    'null' => null,
];
$message = 'Message For Test Purposes';

$raw = \CygnusResponseHelper\Response::raw($response)->array();
$answer = \CygnusResponseHelper\Response::answer(true, $response)->array();
$success = \CygnusResponseHelper\Response::success($response, $message)->array();
$fail = \CygnusResponseHelper\Response::fail($message, $response)->array();

echo \CygnusResponseHelper\Response::raw([
    'raw' => $raw,
    'answer' => $answer,
    'success' => $success,
    'fail' => $fail,
])->xml();
