<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\Cases;

use CygnusResponseHelper\Response;
use PHPUnit\Framework\TestCase;

final class SuccessJsonTest extends TestCase
{
    public function testValid()
    {
        $data = [
            555,
            'aaa',
            'string' => 'String',
            'int' => 123,
            'float' => 13.13,
            'bool' => false,
            'array' => [
                666,
                'bbb',
                'sub_array' => [
                    777,
                    'ccc',
                ],
            ],
            'object' => new \stdClass(),
            'null' => null,
        ];

        $message = 'Message For Test Purposes';

        $response = Response::success($data, $message);

        $this->assertJson($response->json());
        $this->assertJsonStringEqualsJsonString(
            '{
                "message": "Message For Test Purposes",
                "data": {
                    "0": 555,
                    "1": "aaa",
                    "string": "String",
                    "int": 123,
                    "float": 13.13,
                    "bool": false,
                    "array": {
                        "0": 666,
                        "1": "bbb",
                        "sub_array": [
                            777,
                            "ccc"
                        ]
                    },
                    "object": {},
                    "null": null
                },
                "success": true
            }',
            $response->json()
        );
    }
}
