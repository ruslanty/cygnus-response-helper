<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\Cases;

use CygnusResponseHelper\Response;
use PHPUnit\Framework\TestCase;

final class SuccessArrayTest extends TestCase
{
    public function testValid()
    {
        $data = [
            555,
            'aaa',
            'string' => 'String',
            'int' => 123,
            'float' => 13.13,
            'bool' => false,
            'array' => [
                666,
                'bbb',
                'sub_array' => [
                    777,
                    'ccc',
                ],
            ],
            'object' => new \stdClass(),
            'null' => null,
        ];

        $message = 'Message For Test Purposes';

        $response = Response::success($data, $message);

        $this->assertArrayHasKey('success', $response->getResponse());
        $this->assertArrayHasKey('success', $response->array());
    }
}
