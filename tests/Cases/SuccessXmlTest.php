<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\Cases;

use CygnusResponseHelper\Response;
use PHPUnit\Framework\TestCase;

final class SuccessXmlTest extends TestCase
{
    public function testValid()
    {
        $data = [
            555,
            'aaa',
            'string' => 'String',
            'int' => 123,
            'float' => 13.13,
            'bool' => false,
            'array' => [
                666,
                'bbb',
                'sub_array' => [
                    777,
                    'ccc',
                ],
            ],
            'object' => new \stdClass(),
            'null' => null,
        ];

        $message = 'Message For Test Purposes';

        $response = Response::success($data, $message);

        $this->assertXmlStringEqualsXmlString(
            '<?xml version="1.0"?>
            <response>
                <message>Message For Test Purposes</message>
                <data>
                    <item_0>555</item_0>
                    <item_1>aaa</item_1>
                    <string>String</string>
                    <int>123</int>
                    <float>13.13</float>
                    <bool>false</bool>
                    <array>
                        <item_0>666</item_0>
                        <item_1>bbb</item_1>
                        <sub_array>
                            <item_0>777</item_0>
                            <item_1>ccc</item_1>
                        </sub_array>
                    </array>
                    <object/>
                    <null>null</null>
                </data>
                <success>true</success>
            </response>',
            $response->xml()
        );
    }
}
