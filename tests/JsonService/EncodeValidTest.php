<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\JsonService;

use CygnusResponseHelper\Services\JsonService;
use PHPUnit\Framework\TestCase;

final class EncodeValidTest extends TestCase
{
    public function testEncodeNullValid()
    {
        $data = null;
        $json = JsonService::encode($data);

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('null', $json);
    }

    public function testEncodeStringValid()
    {
        $data = '';
        $json = JsonService::encode($data);

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('""', $json);
    }

    public function testEncodeIntValid()
    {
        $data = 0;
        $json = JsonService::encode($data);

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('0', $json);
    }

    public function testEncodeBoolValid()
    {
        $data = false;
        $json = JsonService::encode($data);

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('false', $json);
    }

    public function testEncodeArrayValid()
    {
        $data = [];
        $json = JsonService::encode($data);

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('[]', $json);
    }

    public function testEncodeObjectValid()
    {
        $data = new \stdClass();
        $json = JsonService::encode($data);

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('{}', $json);
    }
}
