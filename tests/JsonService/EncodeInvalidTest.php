<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\JsonService;

use CygnusResponseHelper\Exceptions\JsonException;
use CygnusResponseHelper\Services\JsonService;
use PHPUnit\Framework\TestCase;

final class EncodeInvalidTest extends TestCase
{
    public function testEncodeInvalid()
    {
        $this->expectException(JsonException::class);

        $data = "\xB1\x31";
        JsonService::encode($data);

        $this->expectExceptionCode(json_last_error());
        $this->expectExceptionMessage(json_last_error_msg());
    }
}
