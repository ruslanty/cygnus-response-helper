<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\JsonService;

use CygnusResponseHelper\Services\JsonService;
use PHPUnit\Framework\TestCase;

final class DecodeValidTest extends TestCase
{
    public function testDecodeNullValid()
    {
        $data = 'null';
        $result = JsonService::decode($data);

        $this->assertNull($result);
    }

    public function testDecodeStringValid()
    {
        $data = '""';
        $result = JsonService::decode($data);

        $this->assertIsString($result);
    }

    public function testDecodeIntValid()
    {
        $data = '0';
        $result = JsonService::decode($data);

        $this->assertIsInt($result);
    }

    public function testDecodeBoolValid()
    {
        $data = 'false';
        $result = JsonService::decode($data);

        $this->assertIsBool($result);
    }

    public function testDecodeArrayValid()
    {
        $data = '[]';
        $result = JsonService::decode($data);

        $this->assertIsArray($result);
    }

    public function testDecodeObjectValid()
    {
        $data = '{}';
        $result = JsonService::decode($data);

        $this->assertIsArray($result);
    }
}
