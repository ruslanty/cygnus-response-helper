<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests\JsonService;

use CygnusResponseHelper\Exceptions\JsonException;
use CygnusResponseHelper\Services\JsonService;
use PHPUnit\Framework\TestCase;

final class DecodeInvalidTest extends TestCase
{
    public function testDecodeInvalid()
    {
        $this->expectException(JsonException::class);

        $data = '{';
        JsonService::decode($data);

        $this->expectExceptionCode(json_last_error());
        $this->expectExceptionMessage(json_last_error_msg());
    }
}
