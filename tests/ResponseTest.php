<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Tests;

use CygnusResponseHelper\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    public function testResponse()
    {
        $this->assertInstanceOf(Response::class, new Response());
    }
}
