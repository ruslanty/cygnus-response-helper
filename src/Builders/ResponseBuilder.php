<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Builders;

use CygnusResponseHelper\Decorators\ResponseDecorator;
use CygnusResponseHelper\Exceptions\DecoratorException;
use function call_user_func_array;
use function sprintf;
use function ucfirst;

/**
 * @method array array()
 * @method string json(int $options = 0)
 * @method string xml()
 * @method void die(int $responseCode = 200, int $options = 0)
 */
final class ResponseBuilder
{
    private $response;

    public function __construct(array $response)
    {
        $this->response = $response;
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function __call(string $name, array $arguments)
    {
        $decoratorClass = sprintf('%s\\%sFormat', ResponseDecorator::getDecoratorPath(), ucfirst($name));

        if (!class_exists($decoratorClass)) {
            throw new DecoratorException(sprintf('Decorator class "%s" not found.', $decoratorClass));
        }

        $decorator = new $decoratorClass($this);

        if (!$decorator instanceof ResponseDecorator) {
            throw new DecoratorException(sprintf('Decorator class must be instance of %s', ResponseDecorator::class));
        }

        return call_user_func_array([$decorator, 'format'], $arguments);
    }
}
