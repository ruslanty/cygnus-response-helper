<?php

declare(strict_types=1);

namespace CygnusResponseHelper;

use CygnusResponseHelper\Builders\ResponseBuilder;
use function array_filter;
use function array_merge;
use function compact;

final class Response
{
    public static function raw(array $data = []): ResponseBuilder
    {
        return new ResponseBuilder($data);
    }

    public static function answer(bool $result, array $data = []): ResponseBuilder
    {
        $response = [
            'success' => $result,
        ];

        if ($data) {
            $response = array_merge($data, $response);
        }

        return self::raw($response);
    }

    public static function success(array $data = [], string $message = ''): ResponseBuilder
    {
        $response = self::prepareData($message, $data);

        return self::answer(true, $response);
    }

    public static function fail(string $message = '', array $data = []): ResponseBuilder
    {
        $response = self::prepareData($message, $data);

        return self::answer(false, $response);
    }

    private static function prepareData(string $message, array $data): array
    {
        return array_filter(compact('message', 'data'));
    }
}
