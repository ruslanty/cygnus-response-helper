<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Services;

use SimpleXMLElement;
use function is_array;
use function is_bool;
use function is_null;
use function is_numeric;
use function is_object;
use function simplexml_load_string;
use function sprintf;

final class XmlService
{
    public static function encode(array $data, SimpleXMLElement $xml)
    {
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $key = sprintf('item_%s', $key);
            }

            if (is_object($value)) {
                $value = (array)$value;
            }

            if (is_array($value)) {
                self::encode($value, $xml->addChild($key));

                continue;
            }

            if (is_bool($value)) {
                $value = $value ? 'true' : 'false';
            } elseif (is_null($value)) {
                $value = 'null';
            } else {
                $value = (string)$value;
            }

            $xml->addChild($key, $value);
        }
    }

    public static function decode(string $data): array
    {
        $xml = simplexml_load_string($data);
        $json = JsonService::encode($xml);

        return JsonService::decode($json);
    }
}
