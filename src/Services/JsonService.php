<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Services;

use CygnusResponseHelper\Exceptions\JsonException;
use function json_decode;
use function json_encode;
use function json_last_error;
use function json_last_error_msg;
use const JSON_ERROR_NONE;

final class JsonService
{
    public static function encode($data, int $options = 0, int $depth = 512): string
    {
        $json = json_encode($data, $options, $depth);

        self::throwExceptionOnError();

        return $json;
    }

    public static function decode(string $json, int $options = 0, bool $returnArray = true, int $depth = 512)
    {
        $array = json_decode($json, $returnArray, $depth, $options);

        self::throwExceptionOnError();

        return $array;
    }

    private static function throwExceptionOnError()
    {
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new JsonException(json_last_error_msg(), json_last_error());
        }
    }
}
