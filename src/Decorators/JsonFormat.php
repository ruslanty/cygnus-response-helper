<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Decorators;

use CygnusResponseHelper\Services\JsonService;

final class JsonFormat extends ResponseDecorator
{
    public function format(int $options = 0): string
    {
        return JsonService::encode($this->responseBuilder->getResponse(), $options);
    }
}
