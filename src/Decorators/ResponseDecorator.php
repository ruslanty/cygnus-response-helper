<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Decorators;

use CygnusResponseHelper\Builders\ResponseBuilder;

abstract class ResponseDecorator
{
    protected $responseBuilder;

    public function __construct(ResponseBuilder $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }

    public static function getDecoratorPath(): string
    {
        return __NAMESPACE__;
    }

    abstract public function format();
}
