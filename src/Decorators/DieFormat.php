<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Decorators;

use function header;
use function headers_sent;
use function http_response_code;

final class DieFormat extends ResponseDecorator
{
    public function format(int $responseCode = 200, int $options = 0)
    {
        if (!headers_sent()) {
            http_response_code($responseCode);
            header('Content-Type: application/json');
        }

        die($this->responseBuilder->json($options));
    }
}
