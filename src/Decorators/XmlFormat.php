<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Decorators;

use CygnusResponseHelper\Services\XmlService;
use SimpleXMLElement;
use function header;
use function headers_sent;

final class XmlFormat extends ResponseDecorator
{
    public function format(): string
    {
        if (!headers_sent()) {
            header('Content-Type: text/xml');
        }

        $response = $this->responseBuilder->getResponse();
        $xml = new SimpleXMLElement('<response/>');

        XmlService::encode($response, $xml);

        return $xml->asXML();
    }
}
