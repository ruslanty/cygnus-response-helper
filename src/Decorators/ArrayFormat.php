<?php

declare(strict_types=1);

namespace CygnusResponseHelper\Decorators;

final class ArrayFormat extends ResponseDecorator
{
    public function format(): array
    {
        return $this->responseBuilder->getResponse();
    }
}
